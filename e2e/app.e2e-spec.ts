import { DmAppPage } from './app.po';

describe('dm-app App', () => {
  let page: DmAppPage;

  beforeEach(() => {
    page = new DmAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
