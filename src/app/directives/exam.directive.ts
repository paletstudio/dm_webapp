import { Directive, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
	selector: '[exam]',
})
export class ExamDirective {
	@Input() examid;

	constructor(
		private router: Router,
	) {
	}

	@HostListener('click')
	goToExamPage() {
		this.router.navigate(['/exam', this.examid]);
	}

}
