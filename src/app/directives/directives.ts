import { NgModule } from '@angular/core';
import { ExamDirective } from './exam.directive';

@NgModule({
	imports: [],
	declarations: [
		ExamDirective
	],
	exports: [
		ExamDirective
	]
})
export class CommonDirectives { }
