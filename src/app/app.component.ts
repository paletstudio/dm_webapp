import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { SessionService } from './services/session.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
	public firstTimeVisited: boolean;

	constructor(
		private storage: LocalStorageService,
		private session: SessionService,
		private router: Router
	) {
		this.firstTimeVisited = (this.storage.get('firstTimeVisited') === null ? true : false);

	}

	ngOnInit() {
		if (this.firstTimeVisited) {
			setTimeout(() => {
				this.firstTimeVisited = false;
				this.storage.set('firstTimeVisited', false);
			}, 3000);
		} else {
			this.storage.set('firstTimeVisited', false);
			this.firstTimeVisited = false;
		}
	}
}
