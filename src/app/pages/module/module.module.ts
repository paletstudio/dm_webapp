import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { CommonComponents } from '../../components/components';
import { CommonDirectives } from '../../directives/directives';
import { SwiperModule } from 'angular2-useful-swiper';

import { ModuleRouteModule, routableComponents } from './module.routing';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ModuleRouteModule,
		HttpModule,
		CommonComponents,
		CommonDirectives,
		SwiperModule,
	],
	declarations: [routableComponents],
	providers: []
})
export class ModuleModule { }
