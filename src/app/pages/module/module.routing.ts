import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModuleComponent } from './module.component';

export const routableComponents = [
	ModuleComponent
];
const routes: Routes = [
	{
		path: '',
		component: ModuleComponent
    }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ModuleRouteModule { }
