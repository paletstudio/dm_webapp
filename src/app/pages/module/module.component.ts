import { Component, OnInit, NgZone, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProgramService } from '../../services/program.service';
import { SessionService } from '../../services/session.service';
import { SwiperComponent } from 'angular2-useful-swiper';
import { LocalStorageService } from 'angular-2-local-storage';
import  Player from '@vimeo/player';

@Component({
	selector: 'module',
	templateUrl: './module.component.html',
	styleUrls: ['./module.component.scss']
})
export class ModuleComponent {
	public currentModule;
	public currentIndex;
	public slide;
	public class;
	public style;
	public styleMenu;
	public contents: any[];
	@ViewChild('swiper') swiper;
	public player: Player;

	constructor(
		private route: ActivatedRoute,
		private program: ProgramService,
		private zone: NgZone,
		private session: SessionService,
		private storage: LocalStorageService
	) {
		this.contents = [];
		this.class = 'content-text first-content';
		this.style = 2;
		this.styleMenu = 2;
		this.route.params.subscribe(async params => {
			if (params['id']) {
				let currentModule = await this.program.getModule(params['id']);
				this.zone.run(() => {
					this.currentModule = currentModule;
					if (this.currentModule.id == 14) {
						this.class += ' evidence';
						this.styleMenu = 2;
					}
					this.checkUnlockModules();
				});
			}
			this.getContents();
		});
	}

	async getContents() {
		let userId = this.session.getUserId();
		let contents = await this.program.getContents(this.currentModule.id, userId);
		contents.forEach(content => {
			content.content_html = content.content_html.replace('main/', '');
		});
		this.zone.run(() => {
			this.contents = contents;
			setTimeout(() => this.getActive())
		});
	}

	getActive() {
		this.currentIndex = this.swiper.swiper.activeIndex;
		this.swiper.swiper.allowTouchMove = false;
		if (this.swiper.swiper.slides.length > 0){
			Object.keys(this.swiper.swiper.slides).forEach((s:any) => {
			// this.swiper.swiper.slides.forEach((s: Element) => {
				if (this.swiper.swiper.slides[s].classList){
					if (this.swiper.swiper.slides[s].classList.contains('swiper-slide-active')) {
						this.slide = this.swiper.swiper.slides[s];
						this.getVideoAndPlayer(this.slide);
						this.getClassOfElement(this.slide);
						return;
					}
				}
			});
		}
	}

	getClassOfElement(element: Element) {
		let object = element.querySelectorAll('div.swiper-slide content-view section');
		this.class = object[0].classList[0];
		if (this.class == 'content-exam') {
			this.style = 1;
			this.styleMenu = 1;
		} else if(this.class == 'content-text-pdf' || this.class == 'content-fact') {
			this.styleMenu = 2;
		} else if (object[0].classList.contains('intro')) {
			this.class += ' first-content';
			this.styleMenu = 2;
		} else if (object[0].classList.contains('evidence')) {
			this.styleMenu = 2;
			this.class += ' evidence';
		} else if(object[0].classList.contains('first-content') || this.class === 'content-pdf' || this.class === 'content-video' || this.class === 'content-video-tip') {
			this.styleMenu = 2;
		} else {
			this.style = 2;
			this.styleMenu = 1;
		}
	}

	swipe() {
		// Se hace time out para que entre en el ciclo de angular debido a que
		// el evento swipe se implementa con Hammer.js
		setTimeout(() => this.getActive())
	}

	checkUnlockModules() {
		if (this.storage.get('currentBlock') === 1 && this.currentModule.order === 1) {
			this.program.unlockAccess('block', 2).subscribe(res => {});
			this.program.doneAccess('block', 1).subscribe(res => {});
		} else if (this.storage.get('currentBlock') === 4 && this.currentModule.order === 1) {
			this.program.doneAccess('block', 4).subscribe(res => {});
		}
	}

	@HostListener('document:keydown', ['$event'])
	handleArrows(event: KeyboardEvent) {
		if (event.keyCode === 39){ // Right Key
			this.slideNext(event);
		}
		if (event.keyCode === 37){ // Left Key
			this.slidePrev(event);
		}
	}

	slideNext(event: Event) {
		event.stopPropagation();
		if (this.player) {
			this.player.pause();
		}
		this.swiper.swiper.slideNext();
		this.getActive();
	}

	slidePrev(event: Event) {
		event.stopPropagation();
		if (this.player) {
			this.player.pause();
		}
		this.swiper.swiper.slidePrev();
		this.getActive();
	}

	getVideoAndPlayer(elem: Element) {
		let iframe = elem.querySelectorAll('.content .video iframe');
		if (iframe.length > 0) {
			this.player = new Player(iframe[0]);
		}
	}

}
