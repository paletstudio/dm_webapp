import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { environment } from 'environments/environment';
import { Observable, Observer } from 'rxjs';
import { LocalStorageService } from 'angular-2-local-storage';
import { JwtHelper } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SessionService } from '../../services/session.service';

@Injectable()
export class LoginService {
	public endpoint: string = environment.apiUrl;
	constructor(
		private http: Http,
		private storage: LocalStorageService,
		private jwt: JwtHelper,
		private router: Router,
		private session: SessionService
	) { }

	login(user): Observable<any> {
		return Observable.create((observer: Observer<any>) => {
			this.http.post(this.endpoint + 'login', user).map(this.extractData).catch(this.handleError)
			.subscribe( res => {
				let decoded = this.jwt.decodeToken(res.data);
				let user = decoded.user;
				this.storage.set('token', res.data);
				this.storage.set('user', user);
				this.session.getUserSession();
				observer.next(res.msg);
				observer.complete();
			}, error => {
				observer.error(error);
				observer.complete();
			});
		});
	}

	logout() {
		return Observable.create(observer => {
			let headers = new Headers;
			headers.append('Authorization', 'Bearer ' + this.storage.get('token'));
			this.http.post(this.endpoint + 'logout', {}, {headers: headers}).map(this.extractData).catch(this.handleError)
			.subscribe(res => {
				this.storage.remove('token', 'user');
				observer.next(true);
				observer.complete();
				this.router.navigate(['/login']);
			}, error => {
				this.storage.remove('token', 'user');
				observer.next(false);
				observer.complete();
				this.router.navigate(['/login']);
			});
		});
	}

	isLogged() {
		return !!this.storage.get('token');
	}

	forgotPassword(email: string) {
		return this.http.post(this.endpoint + 'user/forgotpass', {email});
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || [];
	}

	private handleError(error: Response | any) {
        let body = error.json();
		return Observable.throw(body);
	}
}
