import { Component, OnInit } from '@angular/core';
import { Credentials } from './credentials';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import swal from 'sweetalert2';

@Component({
	selector: 'login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	public user: Credentials;
	public forgotemail: string;
	public forgotPass: boolean = false;

	constructor(
		private loginService: LoginService,
		private router: Router,
		private toastr: ToastrService
	) {
		this.user = new Credentials;
		this.forgotemail = '';
	}

	ngOnInit() {
	}

	login() {
		this.loginService.login(this.user).subscribe( res => {
			this.router.navigate(['/menu']);
		}, error => {
			this.toastr.error(error.msg, 'Error');
		});
	}

	activateForgotPass() {
		this.forgotPass = true;
	}

	returnLogin() {
		this.forgotPass = false;
	}

	sendForgotPasswordMail() {
		this.loginService.forgotPassword(this.forgotemail).subscribe( res => {
			swal({
				title: 'Cambio de contraseña',
				text: 'Se han enviado instrucciones a su correo electrónico.',
				type: 'success'
			});
			this.forgotPass = false;
		});
	}

}
