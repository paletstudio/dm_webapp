import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';

import { LoginRouteModule, routableComponents } from './login.routing';
import { LoginService } from './login.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		LoginRouteModule,
		HttpModule,
	],
	declarations: [routableComponents],
	providers: [LoginService]
})
export class LoginModule { }
