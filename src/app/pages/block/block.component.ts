import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ProgramService } from '../../services/program.service';
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../services/session.service';

@Component({
	selector: 'block',
	templateUrl: './block.component.html',
	styleUrls: ['./block.component.scss']
})
export class BlockComponent implements OnInit {
	public currentBlock;
	public modules: any[];
	@ViewChild('swiper') swiper;

	constructor(
		private program: ProgramService,
		private route: ActivatedRoute,
		private session: SessionService,
		private ngzone: NgZone
	) {
		this.modules = [];
		this.route.params.subscribe( async params => {
			if (params['id']) {
				let currentBlock = await this.program.getBlock(params['id']);
				this.ngzone.run( () => {
					this.currentBlock = currentBlock
				});
			}
			this.getModules();
		});
	}

	ngOnInit() {
	}

	ngAfterViewInit() {
		this.swiper.swiper.allowTouchMove = false;
	}

	async getModules() {
		let userId = this.session.getUserId();
		let modules = await this.program.getModules(this.currentBlock.id, userId);
		let chunk = 4;
		let newModules = [];
		let temp;
		let i,j;
		modules.forEach((m, i) => {
			m.last = (i + 1) === modules.length;
		});
		for(i = 0, j = modules.length; i < j; i+= chunk) {
			temp = modules.slice(i, i + chunk);
			newModules.push(temp);
		}
		this.ngzone.run( () => {
			this.modules = newModules;
		});
	}

}
