import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { CommonComponents } from '../../components/components';
import { SwiperModule } from 'angular2-useful-swiper';

import { BlockRouteModule, routableComponents } from './block.routing';


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		BlockRouteModule,
		HttpModule,
		CommonComponents,
		SwiperModule
	],
	declarations: [routableComponents],
	providers: []
})
export class BlockModule { }
