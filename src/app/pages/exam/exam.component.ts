import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProgramService } from '../../services/program.service';
import { SessionService } from '../../services/session.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'environments/environment';

@Component({
	selector: 'exam',
	templateUrl: './exam.component.html',
	styleUrls: ['./exam.component.scss']
})
export class ExamComponent implements OnInit {
	public exam;
	public currentQuestion: number;
	public examComplete: boolean;
	public examSent: boolean;
	public correctAnswers: number;
	public grade: number;
	public uploader: FileUploader;
	public loading: boolean;
	@ViewChild('video') videoInput: ElementRef;
	@ViewChild('image') imageInput: ElementRef;

	constructor(
		private route: ActivatedRoute,
		private program: ProgramService,
		private session: SessionService,
		private zone: NgZone,
		private router: Router
	) { }

	ngOnInit() {
		this.examComplete = false;
		this.examSent = false;
		this.currentQuestion = 0;
		this.correctAnswers = 0;
		this.grade = null;
		this.loading = false;
		this.route.params.subscribe(async params => {
			if (params['id']) {
				let exam = await this.program.getContent(params['id']);
				this.zone.run(() => {
					this.exam = exam;
					this.exam.exam.forEach(question => {
						question.question = question.question.replace(new RegExp('main/', 'g'), '');
					});
					let url: string = environment.apiUrl + this.program.getProgramUrl('module', true) + '/exam/video';
					this.uploader = new FileUploader({
						url: url,
						itemAlias: 'video',
						authToken: `Bearer ${this.session.getToken()}`,
						method: 'POST',
						additionalParameter: {
							question_id: this.exam.exam[this.currentQuestion].id,
							user_id: this.session.getUserId()
						}
					});
				});
			}
		});
	}

	answerQuestion(answer: any, event: Event) {
		this.applyCorrectOrIncorrectClass(answer, event)
		setTimeout(() => {
			this.exam.exam[this.currentQuestion].answer = answer;
			if (this.exam.exam[this.currentQuestion].correct_answer_id == answer) {
				this.correctAnswers++;
			}
			if (this.currentQuestion < this.exam.exam.length) {
				this.currentQuestion++;
				this.isExamComplete();
			}
		}, 500);
	}

	isExamComplete() {
		this.examComplete = this.exam.exam.every(q => {
			return (q.answer !== null) && (q.answer !== undefined)
		});
	}

	finishExam() {
		this.program.saveExam({
			user_id: this.session.getUserId(),
			questions: this.exam.exam
		}).subscribe(res => {
			this.grade = res.data.grade;
			this.examComplete = true;
			this.examSent = true;
		});
	}

	nextModule() {
		if (this.program.isCurrentModuleLastFn()) {
			this.router.navigate(['/menu']);
			return;
		}
		this.router.navigate(['/block', this.program.currentBlock]);
	}

	selectVideo() {
		this.videoInput.nativeElement.click();
	}

	selectImage() {
		this.imageInput.nativeElement.click();
	}

	saveVideo() {
		this.loading = true;
		setTimeout(() => {
			if (this.uploader.queue.length > 0) {
				let file = this.uploader.queue[0];
				file.withCredentials = false;
				file.upload();

				file.onComplete = (response: string, status: number, headers: any) => {
					this.loading = false;
					let res = JSON.parse(response);
					if (res.code === 200){
						this.examSent = true;
						this.examComplete = true;
					} else {
						console.log(res);
					}
				}
			}
		}, 1000);
	}

	saveImage() {
		this.loading = true;
		setTimeout(() => {
			if (this.uploader.queue.length > 0) {
				let file = this.uploader.queue[0];
				file.withCredentials = false;
				file.upload();

				file.onComplete = (response: string, status: number, headers: any) => {
					this.loading = false;
					let res = JSON.parse(response);
					if (res.code === 200){
						this.examSent = true;
						this.examComplete = true;
					} else {
						console.log(res);
					}
				}
			}
		}, 1000);
	}

	applyCorrectOrIncorrectClass(answer: any, event?: any) {
		let button = event.target;
		if (this.exam.exam[this.currentQuestion].correct_answer_id == answer) {
			button.classList.add('correct');
		} else {
			button.classList.add('wrong');
		}
	}

	otherWay() {
		this.program.validateAnswer(this.exam.id, this.exam.exam[0].id).subscribe( res => {
			this.examSent = true;
			this.examComplete = true;
		});
	}

}
