import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { CommonComponents } from '../../components/components';
import { FileUploadModule } from 'ng2-file-upload';

import { ExamRouteModule, routableComponents } from './exam.routing';


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ExamRouteModule,
		HttpModule,
		CommonComponents,
		FileUploadModule,
	],
	declarations: [routableComponents],
	providers: []
})
export class ExamModule { }
