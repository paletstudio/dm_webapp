import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExamComponent } from './exam.component';

export const routableComponents = [
	ExamComponent
];
const routes: Routes = [
	{
		path: '',
		component: ExamComponent
    }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ExamRouteModule { }
