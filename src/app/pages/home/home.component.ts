import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ProgramService } from '../../services/program.service';
import { SessionService } from '../../services/session.service';

@Component({
	selector: 'home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	public blocks: any;

	constructor(
		private program: ProgramService,
		private session: SessionService,
		private router: Router
	) { }

	ngOnInit() {
		this.getBlocks();
	}

	getBlocks() {
		let id = this.session.getUserId();
		console.log(id);
		console.log(this.program);
		this.program.getBlocks(id).subscribe(res => {
			if(res && res.data) {
				res.data.pop();
				this.blocks = res.data;
			}
		});
	}

	goStartHere() {
		this.program.setBlock(6);
		this.router.navigate(['/module', 13]);
		return;
	}

}
