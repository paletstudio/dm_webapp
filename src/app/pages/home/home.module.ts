import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';

import { HomeRouteModule, routableComponents } from './home.routing';
import {BlockNameComponent} from '../../components';
import { CommonComponents } from '../../components/components';


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		HomeRouteModule,
		HttpModule,
		CommonComponents,
	],
	declarations: [routableComponents, BlockNameComponent],
	providers: []
})
export class HomeModule { }
