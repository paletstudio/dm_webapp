import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { LocalStorageModule } from 'angular-2-local-storage';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { SessionService } from './services/session.service';
import { ProgramService } from './services/program.service';
import { API } from './services/api.service';
import { JwtHelper } from 'angular2-jwt';

// Routing
import { AppRoutingModule } from './app.routing';
import { SplashArtComponent } from './components/splash-art/splash-art.component';

export class MyHammerConfig extends HammerGestureConfig {
	overrides = <any>{
		'swipe': { velocity: 0.4, threshold: 20 } // override default settings
	}
}

@NgModule({
	declarations: [
		AppComponent,
		SplashArtComponent,
	],
	imports: [
		BrowserModule,
		HttpModule,
		LocalStorageModule.withConfig({
			prefix: 'dmw',
			storageType: 'localStorage'
		}),
		BrowserAnimationsModule,
		ToastrModule.forRoot(),
		AppRoutingModule,
	],
	providers: [
		SessionService,
		JwtHelper,
		ProgramService,
		API,
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: MyHammerConfig
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
