import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { JwtHelper } from 'angular2-jwt';
import { API } from './api.service';
import { environment } from 'environments/environment';

@Injectable()
export class SessionService {
	public token;
	public user: any;

	constructor(
		private storage: LocalStorageService,
		private jwt: JwtHelper,
		private api: API
	) {
		this.getUserSession();
	}

	getToken() {
		this.getUserSession();
		return this.token;
	}

	getUserId(){
		this.getUserSession();
		if(this.user) {
			return this.user.id;
		}
	}

	getUser() {
		this.getUserSession();
		return this.user;
	}

	getUserProfilePic() {
		return environment.apiUrl + `user/${this.getUser().id}/picture?` + (new Date());
	}

	isValidSession(): boolean {
		this.getToken();
		return !!this.token;
	}

	changePassword(userId: number, data: any) {
		return this.api.patch(`user/${userId}/password`, data);
	}

	changeProfilePic(userId: number, image: any) {
		return this.api.post(`user/${userId}/picture`, {profile_pic: image});
	}

	removeUserSession() {
		this.user = null;
		this.token = null;
	}

	getUserSession() {
		this.user = this.storage.get('user');
		this.token = this.storage.get('token');
	}

}
