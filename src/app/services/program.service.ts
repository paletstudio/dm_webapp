import { Injectable } from '@angular/core';
import { API } from './api.service';
import 'rxjs/add/operator/toPromise';
import { LocalStorageService } from 'angular-2-local-storage';
import { SessionService } from './session.service';

@Injectable()
export class ProgramService {
	public currentCourse;
	public currentBlock;
	public currentModule;
	public currentContent;
	public isCurrentModuleLast: boolean;

	constructor(
		private api: API,
		private storage: LocalStorageService,
		private session: SessionService
	) {
		this.isCurrentModuleLast = false;
		this.storage.set('isCurrentModuleLast', false);
		this.currentCourse = 1;
		this.storage.set('currentCourse', this.currentCourse);
		this.currentBlock = this.storage.get('currentBlock');
		this.currentModule = this.storage.get('currentModule');
		this.currentContent = this.storage.get('currentContent');
	}

	setContent(id: number) {
		this.currentContent = id;
	}

	setBlock(id: number) {
		this.currentBlock = id;
		this.storage.set('currentBlock', this.currentBlock);
	}

	getCourse() {
		return this.api.get(this.getProgramUrl('course', true));
	}

	getBlocks(userId?) {
		let url = this.getProgramUrl('block') + (userId ? `?user_id=${userId}` : '');
		return this.api.get(url);
	}

	getBlock(id, userId?) {
		this.currentBlock = id;
		this.storage.set('currentBlock', this.currentBlock);
		let url = this.getProgramUrl('block', true) + (userId ? `?user_id=${userId}` : '');
		return this.api.get(url).toPromise().then(result => {
			return result.data
		});
	}

	getModules(blockId?, userId?) {
		if (blockId) {
			this.currentBlock = blockId;
			this.storage.set('currentBlock', this.currentBlock);
		}
		let url = this.getProgramUrl('module') + (userId ? `?user_id=${userId}`: '');
		return this.api.get(url).toPromise().then(res => {
			return res.data
		});
	}

	getModule(id, userId?) {
		this.currentModule = id;
		this.storage.set('currentModule', this.currentModule);
		let url = this.getProgramUrl('module', true) + (userId ? `?user_id=${userId}` : '');
		return this.api.get(url).toPromise().then( res => res.data);
	}

	getContents(moduleId, userId?) {
		if (moduleId) {
			this.currentModule = moduleId;
			this.storage.set('currentModule', this.currentModule);
		}
		let url = this.getProgramUrl('content') + (userId ? `?user_id=${userId}`: '');
		return this.api.get(url).toPromise().then(res => res.data);
	}

	getContent(id, userId?) {
		this.currentContent = id;
		this.storage.set('currentContent', this.currentContent);
		let url = this.getProgramUrl('content', true) + (userId ? `?user_id=${userId}`: '');
		return this.api.get(url).toPromise().then( res => res.data);
	}

	saveExam(exam: any) {
		return this.api.post(this.getProgramUrl('module', true) + '/exam', exam);
	}

	sendPDF2Email(body: any) {
		return this.api.post(this.getProgramUrl('content', true) + '/pdf', body);
	}

	downloadPDF() {
		return this.getProgramUrl('content', true) + '/pdf';
	}

	checkIfLastCourseModule(courseId: number, blockId: number, moduleId: number) {
		return courseId === 1 && blockId === 3 && moduleId === 12;
	}

	unlockAccess(level: string, objectId: number) {
		return this.api.post(this.getProgramUrl(level) + '/' + objectId + '/unlock?user_id=' + this.session.getUserId());
	}

	doneAccess(level, objectId) {
		return this.api.post(this.getProgramUrl(level) + '/' + objectId + '/done?user_id=' + this.session.getUserId());
	}

	validateAnswer(contentId: any, questionId: any) {
		return this.api.post(this.getProgramUrl('content') + `/${contentId}/validate`, {user_id: this.session.getUserId(), question_id: questionId});
	}

	currentModuleLast(val: boolean) {
		this.isCurrentModuleLast = val;
		this.storage.set('isCurrentModuleLast', val);
	}
	isCurrentModuleLastFn() {
		return this.storage.get('isCurrentModuleLast');
	}

	getProgramUrl(level: string, detail?: boolean) {
		if (level === 'course') {
			return 'course' + (detail ? '/' + this.currentCourse : '');
		} else if (level === 'block') {
			return this.getProgramUrl('course', true) + '/block' + (detail ? '/' + this.currentBlock : '');
		} else if (level === 'module') {
			return this.getProgramUrl('block', true) + '/module' + (detail ? '/' + this.currentModule : '');
		} else if (level === 'content') {
			return this.getProgramUrl('module', true) + '/content' + (detail ? '/' + this.currentContent : '');
		} else {
			return '';
		}
	}

}
