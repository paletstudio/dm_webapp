import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { LocalStorageService } from 'angular-2-local-storage';
import * as _ from 'lodash';
import { JwtHelper } from 'angular2-jwt';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class API {
    headers: any;
    apiUrl: string = environment.apiUrl;
	ref = this;

    constructor(
		private http: Http,
		private storage: LocalStorageService,
		private router: Router,
		private toastr: ToastrService,
		private jwthelper: JwtHelper
	) {
    }

    public get(url: string): Observable<any> {
		return this.request('get', url);
    }

    public post(url: string, data: any = {}): Observable<any> {
		return this.request('post', url, {body: data});
    }

    public put(url: string, data: any = {}): Observable<any> {
		return this.request('put', url, {body: data});
    }

    public patch(url: string, data: any = {}): Observable<any> {
		return this.request('patch', url, {body: data});
    }

	public delete(url: string): Observable<any> {
		return this.request('get', url);
	}

	public request(type: string, url: string, options?: any): Observable<any> {
		let token:string = this.storage.get<string>('token');
		if (this.jwthelper.isTokenExpired(token)) {
			// return Observable.create( observer => {
			return Observable.create( observer => {
			// 	let headers = new Headers;
			// 	headers.append('Authorization', 'Bearer ' + token);
			// 	this.http.get(this.apiUrl + 'refreshtoken', {headers: headers}).map(this.extractData).catch(this.handleError).subscribe(res => {
			// 		token = res.data;
			// 		this.storage.set('token', token);
			// 		let headers = new Headers;
			// 		headers.append('Authorization', 'Bearer ' + token);
			// 		let defaults = {
			// 			headers: headers,
			// 			method: type
			// 		};
			//
			// 		let opts = _.merge(defaults, options);
			// 		return this.http.request(this.apiUrl + url, opts).map(this.extractData).catch(this.handleError.bind(this)).subscribe( res => observer.next(res), err => observer.error(err));
			// 	}, err => {
					this.toastr.info('Sesión expirada');
					this.storage.clearAll();
					observer.next('No hay sesion');
					this.router.navigate(['login']);
				// });
				});
			// });
		} else {
			let headers = new Headers;
			headers.append('Authorization', 'Bearer ' + token);
			let defaults = {
				headers: headers,
				method: type
			};

			let opts = _.merge(defaults, options);
			return this.http.request(this.apiUrl + url, opts).map(this.extractData).catch(this.handleError.bind(this));
		}
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || [];
	}

	private handleError(error: Response | any) {
        let body = error.json();
		if (error.status == 401) {
			this.toastr.info('Sesión expirada');
			this.storage.clearAll();
			this.router.navigate(['login']);
		}
		return Observable.throw(body);
	}
}
