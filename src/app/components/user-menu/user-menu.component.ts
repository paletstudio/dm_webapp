import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { LoginService } from '../../pages/login/login.service';
import { SessionService } from '../../services/session.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'user-menu',
	templateUrl: './user-menu.component.html',
	styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
	public menuState: boolean; // false => closed, true => opened
	@Input() style: number;

	public password: string;
	public newPassword: string;
	public repeatPassword: string;
	public isChangingPass: boolean;
	public isChangingProfilePic: boolean;
	public message: string;
	public error: boolean;
	public changedPassword: boolean;
	public wrongActualPass: boolean;
	public loading: boolean;
	public image: any;
	public profilePicUrl: string;
	public user: any;
	@ViewChild('form') form;
	@ViewChild('fileuploader') uploader: ElementRef;

	constructor(
		private loginService: LoginService,
		private session: SessionService,
		private toastr: ToastrService
	) {
		this.style = 1;
		this.isChangingPass = false;
		this.changedPassword = false;
		this.wrongActualPass = false;
		this.isChangingProfilePic = false;
		this.user = this.session.getUser();
		this.profilePicUrl = this.session.getUserProfilePic();
	}

	ngOnInit() {
		this.menuState = false;
	}

	toggleMenu() {
		this.menuState = !this.menuState;
		this.isChangingPass = false;
	}

	changeProfilePic() {
		this.loading = true;
		this.session.changeProfilePic(this.user.id, this.image).subscribe( res => {
			setTimeout(() => {
				this.toastr.success(res.msg, 'Éxito');
				this.loading = false;
				this.profilePicUrl = this.session.getUserProfilePic();
				this.isChangingProfilePic = false;
			}, 3000)
		});
	}

	changePassword(): void {
		this.wrongActualPass = false;
		if (this.password === this.newPassword) {
			this.message = 'La contraseña nueva debe ser diferente a la actual';
			this.error = true;
			return;
		}
		if (this.newPassword !== this.repeatPassword) {
			this.message = 'Las contraseñas deben coincidir';
			this.error = true;
			return;
		}
		this.session.changePassword(this.user.id, { password: this.password, new_password: this.newPassword }).subscribe(res => {
			this.error = false;
			this.changedPassword = true;
			this.message = res.msg;
			this.changedPassword = true;
			this.wrongActualPass = false;
			this.form.reset();
			this.toastr.success(res.msg, 'Éxito');
			setTimeout(() => {
				this.changedPassword = false;
				this.error = false;
				this.isChangingPass = false;
			}, 2000);
		}, error => {
			this.message = error.msg;
			this.error = false;
			this.wrongActualPass = true;
		});
	}

	showPasswordChange() {
		this.isChangingPass = true;
	}

	hidePasswordChange() {
		this.isChangingPass = false;
	}

	logout() {
		this.loginService.logout().subscribe(res => {
			console.log(res);
		});
	}

	showProfilePicChange() {
		this.isChangingProfilePic = true;
	}

	hideProfilePicChange() {
		this.isChangingProfilePic = false;
	}

	changeListener($event): void {
		this.readThis($event.target);
	}

	readThis(inputValue: any): void {
		var file: File = inputValue.files[0];
		var myReader: FileReader = new FileReader();

		myReader.onloadend = (e) => {
			this.image = myReader.result;
		}
		myReader.readAsDataURL(file);
	}

	focusFileUploader() {
		this.uploader.nativeElement.click();
	}

}
