import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ProgramService } from '../../services/program.service';

@Component({
	selector: 'module-view',
	templateUrl: './module-view.component.html',
	styleUrls: ['./module-view.component.scss']
})
export class ModuleViewComponent implements OnInit {
	@Input() data;
	public moduleIndex: string;
	public moduleName: string;
	public moduleDescription: string;

	constructor(
		private router: Router,
		private program: ProgramService
	) { }

	ngOnInit() {
		this.delegateData();
	}

	delegateData() {
		let moduleHead = this.data.name.split(':');
		this.moduleIndex = moduleHead[0];
		this.moduleName = moduleHead[1];
		this.moduleDescription = this.data.description;
	}

	navigate() {
		if (this.data.last) {
			this.program.currentModuleLast(true);
		} else {
			this.program.currentModuleLast(false);
		}
		this.router.navigate(['/module', this.data.id]);
	}

}
