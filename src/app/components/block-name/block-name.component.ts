import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';
import { ProgramService } from '../../services/program.service';

@Component({
	selector: 'block-name',
	templateUrl: './block-name.component.html',
	styleUrls: ['./block-name.component.scss']
})
export class BlockNameComponent implements OnInit {
	@Input() block;

	constructor(
		private router: Router,
		private program: ProgramService
	) {
		this.block = [];
	}

	ngOnInit() {
	}

	goTo(id) {
		if (id == 1) {
			this.program.setBlock(1);
			this.router.navigate(['/module', 1]);
			return;
		}
		if (id == 4) {
			this.program.setBlock(4);
			this.router.navigate(['/module', 14]);
			return;
		}
		this.router.navigate(['/block', id]);
	}

}
