import { Component, OnInit, Input } from '@angular/core';
import { ProgramService } from '../../services/program.service';
import { SessionService } from '../../services/session.service';
import swal from 'sweetalert2';
import { environment } from 'environments/environment';

@Component({
	selector: 'button-pdf',
	templateUrl: './button-pdf.component.html',
	styleUrls: ['./button-pdf.component.scss']
})
export class ButtonPdfComponent implements OnInit {
	@Input() type: string;
	@Input() text: string;
	@Input() examid: number;
	public loading: boolean;

	constructor(
		private program: ProgramService,
		private session: SessionService
	) {
		this.type = 'pdf';
		this.loading = false;
	}

	ngOnInit() {
		if (this.type === 'email'){
			this.text = 'Envíalo a tu correo electrónico';
		}
	}

	pdfAction() {
		this.program.setContent(this.examid);
		if (this.type === 'email') {
			this.loading = true;
			this.program.sendPDF2Email({
				email: this.session.getUser().email
			}).subscribe( res => {
				this.loading = false;
				swal({
					title: 'Éxito',
					text: 'Se envió el pdf de ésta lección a su correo electrónico.',
					type: 'success'
				});
			}, error => {
				this.loading = false;
				swal({
					title: 'Lo sentimos',
					text: 'Se produjo un problema al enviar la lección a su correo. Contacte con un supervisor o inténtelo más tarde.',
					type: 'error'
				});
			});
		} else {
			let url = environment.apiUrl + this.program.downloadPDF() + '?token=' + this.session.getToken();
			window.open(url, '_system', 'location=yes');
		}
	}

}
