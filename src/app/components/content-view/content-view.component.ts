import { Component, OnInit, Input, ViewChild, ViewContainerRef, ComponentRef, Compiler, NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { CommonDirectives } from '../../directives/directives';
import { CommonComponents } from '../../components/components';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'content-view',
	template: '<ng-template #target class="content"></ng-template>',
	styleUrls: ['./content-view.component.scss']
})
export class ContentViewComponent implements OnInit {

	@Input() content;
	@ViewChild('target', { read: ViewContainerRef }) target: ViewContainerRef;
	// private componentRef:ComponentRef<any>;

	constructor(private compiler: Compiler) { }

	ngOnInit() {
		this.content.content_html = this.content.content_html.replace(new RegExp('go-to-anchor', 'g'), 'p');
		this.content.content_html = this.content.content_html.replace('content="r"', `examid="${this.content.id}"`);
		this.content.content_html = this.content.content_html.replace('content="r"', `[examid]="${this.content.id}"`);
		this.content.content_html = this.content.content_html.replace('ui-sref="main.course.detail({course_id: 1})"', `(click)="goToMenu()"`);
		this.addComponent(this.content.content_html);
	}

	private addComponent(template: string, properties?: any) {
		@Component({
			template: `<div class="content">
				`+ template + `
			</div>`
		})
		class TemplateComponent {
			constructor(private router: Router){}
			goToMenu() {
				this.router.navigate(['/menu'])
			}
		}

		@NgModule({ declarations: [TemplateComponent], imports: [CommonDirectives, CommonComponents, RouterModule] })
		class TemplateModule { }

		const mod = this.compiler.compileModuleAndAllComponentsSync(TemplateModule);
		const factory = mod.componentFactories.find((comp) =>
			comp.componentType === TemplateComponent
		);
		const component = this.target.createComponent(factory);
		Object.assign(component.instance, properties);
		// If properties are changed at a later stage, the change detection
		// may need to be triggered manually:
		// component.changeDetectorRef.detectChanges();
	}
}
