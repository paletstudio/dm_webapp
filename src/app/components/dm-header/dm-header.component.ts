import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'dm-header',
	templateUrl: './dm-header.component.html',
	styleUrls: ['./dm-header.component.scss']
})
export class DmHeaderComponent implements OnInit {
	@Input() title: string;
	@Input() style: number;
	@Input() backArrow: boolean;
	@Output() backArrowChange: EventEmitter<boolean> = new EventEmitter;

	constructor(
		private location: Location,
		private route: ActivatedRoute,
		private router: Router
	) {
		this.style = 1;
		this.backArrow = true;
	}

	ngOnInit() {
	}

	goBack() {
		if (this.router.url.includes('block')){
			this.router.navigate(['/menu']);
		} else {
			this.location.back();
		}
	}

	backArrowChanged() {
		this.backArrowChange.emit(!this.backArrow);
	}

}
