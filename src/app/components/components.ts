import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DmHeaderComponent } from './dm-header/dm-header.component';
import { ModuleViewComponent } from './module-view/module-view.component';
import { ContentViewComponent } from './content-view/content-view.component';
import { GradeComponent } from './grade/grade.component';
import { ButtonPdfComponent } from './button-pdf/button-pdf.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { LoginService } from '../pages/login/login.service';
import { SessionService } from '../services/session.service';
import { ToastrService } from 'ngx-toastr';

@NgModule({
	imports: [CommonModule, FormsModule, RouterModule],
	declarations: [DmHeaderComponent,
		ModuleViewComponent,
		ContentViewComponent,
		GradeComponent,
		ButtonPdfComponent,
		UserMenuComponent
	],
	providers: [
		LoginService,
		SessionService,
		ToastrService
	],
	entryComponents: [],
	exports: [DmHeaderComponent,
		ModuleViewComponent,
		ContentViewComponent,
		GradeComponent,
		ButtonPdfComponent,
		UserMenuComponent
	]
})
export class CommonComponents { }
