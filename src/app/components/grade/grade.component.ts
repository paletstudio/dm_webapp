import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'grade',
	templateUrl: './grade.component.html',
	styleUrls: ['./grade.component.scss']
})
export class GradeComponent implements OnInit {
	@Input() grade: number;
	@Input() size: string;
	@Input() inline: boolean;
	@Input() show: boolean;

	constructor() {
		// Default values
		this.size = '1em';
		this.show = true;
		this.inline = false;
	}

	ngOnInit() {
	}

}
