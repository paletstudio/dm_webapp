import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginActivate, RoutesActivate } from './routing.validators';

export const routes: Routes = [
	{
		path: '',
		redirectTo: 'login',
		pathMatch: 'full',
	},
	{
		path: 'login',
		canActivate: [LoginActivate],
		loadChildren: './pages/login/login.module#LoginModule'
	},
	{
		path: 'menu',
		loadChildren: './pages/home/home.module#HomeModule',
		canActivate: [RoutesActivate]
	},
	{
		path: 'block/:id',
		loadChildren: './pages/block/block.module#BlockModule',
		canActivate: [RoutesActivate]
	},{
		path: 'module/:id',
		loadChildren: './pages/module/module.module#ModuleModule',
		canActivate: [RoutesActivate]
	}, {
		path: 'exam/:id',
		loadChildren: './pages/exam/exam.module#ExamModule',
		canActivate: [RoutesActivate]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		useHash: true
	})],
	exports: [RouterModule],
	providers: [
		LoginActivate,
		RoutesActivate
	]
})
export class AppRoutingModule { }
