import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { SessionService } from './services/session.service';
@Injectable()
export class LoginActivate implements CanActivate{
	constructor(private session: SessionService, private router: Router) { }

	canActivate() {
		if (!this.session.isValidSession()) {
			return true;
		} else {
			this.router.navigate(['menu']);
			return false;
		}
	}
}

@Injectable()
export class RoutesActivate implements CanActivate {

	constructor(private session: SessionService, private router: Router) { }

	canActivate() {
		if (this.session.isValidSession()) {
			return true;
		} else {
			this.router.navigate(['login']);
			return false;
		}
	}

}
